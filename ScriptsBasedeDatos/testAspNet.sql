create DATABASE	testAspnet
GO
USE [testAspnet]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Aplicacion](
	[Id_Aplicacion] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NULL,
 CONSTRAINT [PK_Aplicacion] PRIMARY KEY CLUSTERED 
(
	[Id_Aplicacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Roles](
	[Id_Roles] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[Id_Roles] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuario](
	[Id_Usuario] [int] IDENTITY(1,1) NOT NULL,
	[Nombres] [varchar](100) NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[Id_Usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuarioAppRol](
	[Id_UsuarioAppRol] [int] IDENTITY(1,1) NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Id_Roles] [int] NOT NULL,
	[Id_Aplicacion] [int] NOT NULL,
 CONSTRAINT [PK_UsuarioAppRol] PRIMARY KEY CLUSTERED 
(
	[Id_UsuarioAppRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[UsuarioAppRol]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioAppRol_Aplicacion] FOREIGN KEY([Id_Aplicacion])
REFERENCES [dbo].[Aplicacion] ([Id_Aplicacion])
GO
ALTER TABLE [dbo].[UsuarioAppRol] CHECK CONSTRAINT [FK_UsuarioAppRol_Aplicacion]
GO
ALTER TABLE [dbo].[UsuarioAppRol]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioAppRol_Roles] FOREIGN KEY([Id_Roles])
REFERENCES [dbo].[Roles] ([Id_Roles])
GO
ALTER TABLE [dbo].[UsuarioAppRol] CHECK CONSTRAINT [FK_UsuarioAppRol_Roles]
GO
ALTER TABLE [dbo].[UsuarioAppRol]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioAppRol_Usuario] FOREIGN KEY([Id_Usuario])
REFERENCES [dbo].[Usuario] ([Id_Usuario])
GO
ALTER TABLE [dbo].[UsuarioAppRol] CHECK CONSTRAINT [FK_UsuarioAppRol_Usuario]
GO



GO

INSERT INTO [dbo].[Aplicacion]
           (
           [Descripcion]
		   )
     VALUES
           ('Comentario'),('Usuario'),('Global');
GO

USE [testAspnet]
GO

INSERT INTO [dbo].[Roles]
           ([Descripcion])
     VALUES
           ('Crear'),('Leer'),('Buscar'),('Comentar'),('Aprobar'),('Eliminar')
GO

USE [testAspnet]
GO

INSERT INTO [dbo].[Usuario]
           (
		   [Nombres]
           )
     VALUES
           ('Visitante'),
		   ('Usuario Autenticado'),
		   ('Editor'),
		   ('Administrador')
GO


USE [testAspnet]
GO

INSERT INTO [dbo].[UsuarioAppRol]
           ([Id_Usuario]
           ,[Id_Roles]
           ,[Id_Aplicacion])
     VALUES
           (1,2,3),
		   (1,3,3),
		   (2,2,3),
		   (2,3,3),
		   (2,4,3),
		   (3,2,3),
		   (3,3,3),
		   (3,5,3),
		   (4,2,3),
		   (4,3,3),
		   (4,4,1),
		   (4,1,2),
		   (4,6,2)
GO

