import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import {FormBuilder,FormGroup,Validators} from '@angular/forms'
import {IMyDpOptions} from 'mydatepicker';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { testAspNetServices } from '../services/testAspNetServices';
import { UtilService } from 'mydatepicker/dist/services/my-date-picker.util.service';
import { aplicacion } from '../Models/aplicacion';
import { serializacion } from '../utils/utilidadSerializacion';
import { Roles } from '../Models/roles';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {

  public formulario:FormGroup;
  public aplicaciones:Roles;
  public mensajeapp:string;
  public resTabla:any;
  public idEditar:number;

  @ViewChild("mensaje") mensajemodal: TemplateRef<any>;

  constructor(private fb: FormBuilder, 
    private serv:testAspNetServices,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.formulario = this.fb.group(
      {
        'Roles.Descripcion':[null,Validators.required]
      }      
  );
  this.listar();
  this.idEditar=-1;

  }

modoEdit(id){
  console.log(id);
  this.idEditar = id;
}

agregar(form){
  console.log(form);
this.aplicaciones = serializacion.instancia(new Roles(),form.value);
var request = this.serv.AgregarRoles(this.aplicaciones);
    request.subscribe(
      data=>{
          if(data){
            this.mensajeapp = 'Se actualizo correctamente.';
            this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
          }else{
            this.mensajeapp = 'No se pudo actualizar el registro por error en la información.';
            this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
          }
          this.formulario.reset();
          this.listar();
      },
      error=>{
        this.mensajeapp = 'Error' +JSON.stringify (error);
            this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
      }
  );
}

editar(form){

console.log(form);
var request = this.serv.EditarRoles(form);
    request.subscribe(
      data=>{
          if(data){
            this.mensajeapp = 'Se Edito correctamente.';
            this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
          }else{
            this.mensajeapp = 'No se pudo actualizar el registro por error en la información.';
            this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
          }
          this.formulario.reset();
          this.listar();
          this.idEditar=-1;
      },
      error=>{
        this.mensajeapp = 'Error' +JSON.stringify (error);
            this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
      }
  );
}


borrar(form){
  console.log(form);
  var request = this.serv.BorrarRoles(form);
      request.subscribe(
        data=>{
            if(data){
              this.mensajeapp = 'Se Borro correctamente.';
              this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
            }else{
              this.mensajeapp = 'No se pudo actualizar el registro por error en la información.';
              this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
            }
            this.formulario.reset();
            this.listar();
        },
        error=>{
          this.mensajeapp = 'Error' +JSON.stringify (error);
              this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
        }
    );
  }


listar(){
  
var request = this.serv.ListarRoles();
    request.subscribe(
      data=>{
          if(data){
            console.log(data);
            this.resTabla = data;
          }else{
            this.mensajeapp = 'No se pudo consultar la información.';
            this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
          }
          this.formulario.reset();
      },
      error=>{
        this.mensajeapp = 'Error' + JSON.stringify (error);
            this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
      }
  );
}

}
