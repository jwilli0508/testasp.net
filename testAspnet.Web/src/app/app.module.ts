import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routes,RouterModule} from '@angular/router'
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import {HttpClientModule} from '@angular/common/http'
import {NgbModule,NgbModalModule,NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { MyDatePickerModule } from 'mydatepicker';


import { AppComponent } from './app.component';
import { RolesComponent } from './roles/roles.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { AplicacionesComponent } from './aplicaciones/aplicaciones.component';
import { testAspNetServices } from './services/testAspNetServices';
import { HomeComponent } from './home/home.component';
import { AgregarPermisosComponent } from './agregar-permisos/agregar-permisos.component';

let rutas:Routes = [
  {
    path:'home',component:HomeComponent
  },
  {
    path:'roles',component:RolesComponent
  }
  ,
  {
    path:'usuario',component:UsuarioComponent
  }
  ,
  {
    path:'aplicaciones',component:AplicacionesComponent
  }
  ,
  {
    path:'permisos',component:AgregarPermisosComponent
  }
  ,
  {
    path:'',redirectTo:'/home',pathMatch:'full'
  }
]



@NgModule({
  declarations: [
    AppComponent,
    RolesComponent,
    UsuarioComponent,
    AplicacionesComponent,
    HomeComponent,
    AgregarPermisosComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(rutas),
    NgbModule.forRoot(),
    MyDatePickerModule,
    NgbModule,
    NgbModalModule
  ],
  providers: [
    testAspNetServices,
    NgbActiveModal
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
