import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Headers, RequestOptions } from '@angular/http';

@Injectable()
export class testAspNetServices {
    public urlApp:string;

    constructor(private http: HttpClient) {
        this.urlApp= "http://localhost:60000/";
    }


    public AgregarAplicacion(activosFijos: any) {

        var respuesta = this.http.post(this.urlApp+'app/AddAplicacion',
            activosFijos
            ,
            {
                headers: new HttpHeaders().set('Content-Type', 'application/json')
            }
        );
        return respuesta;
    }


    public EditarAplicacion(activosFijos: any) {

        var respuesta = this.http.post(this.urlApp+'app/EditAplicacion',
            activosFijos
            ,
            {
                headers: new HttpHeaders().set('Content-Type', 'application/json')
            }
        );
        return respuesta;
    }

    public BorrarAplicacion(activosFijos: any) {

        var respuesta = this.http.post(this.urlApp+'app/DeleteAplicacion',
            activosFijos,
            {
                headers: new HttpHeaders().set('Content-Type', 'application/json')
            }
        );
        return respuesta;
    }

    public ListarAplicacion() {

        var respuesta = this.http.post(this.urlApp+'app/GetAllAplicacion'
            ,
            {
                headers: new HttpHeaders().set('Content-Type', 'application/json')
            }
        );
        return respuesta;
    }


    public AgregarRoles(activosFijos: any) {

        var respuesta = this.http.post(this.urlApp+'app/AddRoles',
            activosFijos
            ,
            {
                headers: new HttpHeaders().set('Content-Type', 'application/json')
            }
        );
        return respuesta;
    }


    public EditarRoles(roles: any) {

        var respuesta = this.http.post(this.urlApp+'app/EditRoles',
        roles
            ,
            {
                headers: new HttpHeaders().set('Content-Type', 'application/json')
            }
        );
        return respuesta;
    }

    public BorrarRoles(roles: any) {

        var respuesta = this.http.post(this.urlApp+'app/DeleteRoles',
        roles,
            {
                headers: new HttpHeaders().set('Content-Type', 'application/json')
            }
        );
        return respuesta;
    }

    public ListarRoles() {

        var respuesta = this.http.post(this.urlApp+'app/GetAllRoles'
            ,
            {
                headers: new HttpHeaders().set('Content-Type', 'application/json')
            }
        );
        return respuesta;
    }



    public AgregarUsuario(activosFijos: any) {

        var respuesta = this.http.post(this.urlApp+'app/AddUsuario',
            activosFijos
            ,
            {
                headers: new HttpHeaders().set('Content-Type', 'application/json')
            }
        );
        return respuesta;
    }


    public EditarUsuario(roles: any) {

        var respuesta = this.http.post(this.urlApp+'app/EditUsuario',
        roles
            ,
            {
                headers: new HttpHeaders().set('Content-Type', 'application/json')
            }
        );
        return respuesta;
    }

    public BorrarUsuario(roles: any) {

        var respuesta = this.http.post(this.urlApp+'app/DeleteUsuario',
        roles,
            {
                headers: new HttpHeaders().set('Content-Type', 'application/json')
            }
        );
        return respuesta;
    }

    public ListarUsuario() {

        var respuesta = this.http.post(this.urlApp+'app/GetAllUsuario'
            ,
            {
                headers: new HttpHeaders().set('Content-Type', 'application/json')
            }
        );
        return respuesta;
    }

    public listarUsuarioAppRol() {

        var respuesta = this.http.post(this.urlApp+'app/GetAllUsuarioAppRol'
            ,
            {
                headers: new HttpHeaders().set('Content-Type', 'application/json')
            }
        );
        return respuesta;
    }

//

public AgregarPermiso(activosFijos: any) {

    var respuesta = this.http.post(this.urlApp+'app/AddUsuarioAppRol',
        activosFijos
        ,
        {
            headers: new HttpHeaders().set('Content-Type', 'application/json')
        }
    );
    return respuesta;
}


public EditarPermiso(roles: any) {

    var respuesta = this.http.post(this.urlApp+'app/EditUsuarioAppRol',
    roles
        ,
        {
            headers: new HttpHeaders().set('Content-Type', 'application/json')
        }
    );
    return respuesta;
}

public BorrarPermiso(roles: any) {

    var respuesta = this.http.post(this.urlApp+'app/DeleteUsuarioAppRol',
    roles,
        {
            headers: new HttpHeaders().set('Content-Type', 'application/json')
        }
    );
    return respuesta;
}

public ListarPermiso() {

    var respuesta = this.http.post(this.urlApp+'app/GetAllUsuarioAppRol'
        ,
        {
            headers: new HttpHeaders().set('Content-Type', 'application/json')
        }
    );
    return respuesta;
}
    

}
