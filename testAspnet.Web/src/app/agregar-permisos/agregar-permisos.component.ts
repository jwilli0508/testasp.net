import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import {FormBuilder,FormGroup,Validators} from '@angular/forms'
import {IMyDpOptions} from 'mydatepicker';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { testAspNetServices } from '../services/testAspNetServices';
import { UtilService } from 'mydatepicker/dist/services/my-date-picker.util.service';
import { aplicacion } from '../Models/aplicacion';
import { serializacion } from '../utils/utilidadSerializacion';
import { UsuarioAppRol } from '../Models/UsuarioAppRol';

@Component({
  selector: 'app-agregar-permisos',
  templateUrl: './agregar-permisos.component.html',
  styleUrls: ['./agregar-permisos.component.css']
})
export class AgregarPermisosComponent implements OnInit {

  
  public formulario:FormGroup;
  public aplicaciones:UsuarioAppRol;
  public mensajeapp:string;
  public resTabla:any;
  public idEditar:number;
  public usuarioLs:any;
  public appLs:any;
  public rolesLs:any;

  @ViewChild("mensaje") mensajemodal: TemplateRef<any>;

  constructor(private fb: FormBuilder, 
    private serv:testAspNetServices,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.formulario = this.fb.group(
      {
        'UsuarioAppRol.Id_Usuario':[null,Validators.required],
        'UsuarioAppRol.Id_Roles':[null,Validators.required],
        'UsuarioAppRol.Id_Aplicacion':[null,Validators.required]
      }      
  );
  

  this.listarUsuario();
  this.listarRol();
  this.listarApp();
  this.idEditar=-1;

  this.listar();
  }
  validacion(a){
    console.log(a);
      }
modoEdit(id){
  console.log(id);
  this.idEditar = id;
}




agregar(form){
  console.log(form);
this.aplicaciones = serializacion.instancia(new UsuarioAppRol(),form.value);
var request = this.serv.AgregarPermiso(this.aplicaciones);
    request.subscribe(
      data=>{
          if(data){
            this.mensajeapp = 'Se actualizo correctamente.';
            this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
          }else{
            this.mensajeapp = 'No se pudo actualizar el registro por error en la información.';
            this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
          }
          this.formulario.reset();
          this.listar();
      },
      error=>{
        this.mensajeapp = 'Error' +JSON.stringify (error);
            this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
      }
  );
}

editar(form){

console.log(form);
var request = this.serv.EditarAplicacion(form);
    request.subscribe(
      data=>{
          if(data){
            this.mensajeapp = 'Se Edito correctamente.';
            this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
          }else{
            this.mensajeapp = 'No se pudo actualizar el registro por error en la información.';
            this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
          }
          this.formulario.reset();
          this.listar();
          this.idEditar=-1;
      },
      error=>{
        this.mensajeapp = 'Error' +JSON.stringify (error);
            this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
      }
  );
}


borrar(form){
  console.log(form);
  var request = this.serv.BorrarAplicacion(form);
      request.subscribe(
        data=>{
            if(data){
              this.mensajeapp = 'Se Borro correctamente.';
              this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
            }else{
              this.mensajeapp = 'No se pudo actualizar el registro por error en la información.';
              this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
            }
            this.formulario.reset();
            this.listar();
        },
        error=>{
          this.mensajeapp = 'Error' +JSON.stringify (error);
              this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
        }
    );
  }


listar(){
  
var request = this.serv.listarUsuarioAppRol();
    request.subscribe(
      data=>{
          if(data){
            console.log(data);
            this.resTabla = data;
          }else{
            this.mensajeapp = 'No se pudo consultar la información.';
            this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
          }
          this.formulario.reset();
      },
      error=>{
        this.mensajeapp = 'Error' +JSON.stringify (error);
            this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
      }
  );
}


listarUsuario(){
  
  var request = this.serv.ListarUsuario();
      request.subscribe(
        data=>{
            if(data){
              console.log(data);
              this.usuarioLs = data;
            }else{
              this.mensajeapp = 'No se pudo consultar la información.';
              this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
            }
            this.formulario.reset();
        },
        error=>{
          this.mensajeapp = 'Error' + JSON.stringify (error);
              this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
        }
    );
  }


  listarRol(){
  
    var request = this.serv.ListarRoles();
        request.subscribe(
          data=>{
              if(data){
                console.log(data);
                this.rolesLs = data;
              }else{
                this.mensajeapp = 'No se pudo consultar la información.';
                this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
              }
              this.formulario.reset();
          },
          error=>{
            this.mensajeapp = 'Error' + JSON.stringify (error);
                this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
          }
      );
    }



    
listarApp(){
  
  var request = this.serv.ListarAplicacion();
      request.subscribe(
        data=>{
            if(data){
              console.log('app',data);
              this.appLs = data;
            }else{
              this.mensajeapp = 'No se pudo consultar la información.';
              this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
            }
            this.formulario.reset();
        },
        error=>{
          this.mensajeapp = 'Error' +JSON.stringify (error);
              this.modalService.open( this.mensajemodal, { windowClass: 'dark-modal' });
        }
    );
  }
}
