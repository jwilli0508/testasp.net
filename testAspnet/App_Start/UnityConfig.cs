using System.Web.Http;
using Unity;
using Unity.WebApi;
using testAspnet.BusinessLayer.Interface;
using testAspnet.BusinessLayer.Concrete;

namespace testAspnet
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            container.RegisterType<IUsuario, testAspnet.BusinessLayer.Concrete.Usuario>();
            container.RegisterType<IRoles, testAspnet.BusinessLayer.Concrete.Roles>();
            container.RegisterType<IUsuarioAppRol, testAspnet.BusinessLayer.Concrete.UsuarioAppRol>();
            container.RegisterType<IAplicacion, testAspnet.BusinessLayer.Concrete.Aplicacion>();
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}
