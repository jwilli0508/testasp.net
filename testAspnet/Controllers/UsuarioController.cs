﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using testAspnet.BusinessLayer.Interface;
namespace testAspnet.Controllers
{
    public class UsuarioController : ApiController
    {
        private IUsuario pUsuario;
        public UsuarioController(IUsuario pUsuario)
        {
            this.pUsuario = pUsuario;
        }
        [HttpPost]
        [Route("app/AddUsuario")]
        public bool AddUsuario(Entity.Usuario u)
        {
            return pUsuario.AddUsuario(u);
        }
        [HttpPost]
        [Route("app/EditUsuario")]
        public bool EditUsuario(Entity.Usuario u)
        {
            return pUsuario.EditUsuario(u);
        }
        [HttpPost]
        [Route("app/DeleteUsuario")]
        public bool DeleteUsuario(Entity.Usuario u)
        {
            return pUsuario.DeleteUsuario(u);
        }
        [HttpPost]
        [Route("app/GetUsuario")]
        public List<Entity.Usuario> GetUsuario(Entity.Usuario u)
        {
            return pUsuario.GetUsuario(u);
        }
        [HttpPost]
        [Route("app/GetAllUsuario")]
        public List<Entity.Usuario> GetAllUsuario()
        {
            return pUsuario.GetAllUsuario();
        }
    }
}
