﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using testAspnet.BusinessLayer.Interface;

namespace testAspnet.Controllers
{
    public class AplicacionController : ApiController
    {
        private IAplicacion pAplicacionr;
        public AplicacionController(IAplicacion pAplicacion)
        {
            this.pAplicacionr = pAplicacion;
        }

        [HttpPost]
        [Route("app/AddAplicacion")]
        public bool AddAplicacion(Entity.Aplicacion a)
        {
            return pAplicacionr.AddAplicacion(a);
        }
        [HttpPost]
        [Route("app/EditAplicacion")]
        public bool EditAplicacion(Entity.Aplicacion a)
        {
            return pAplicacionr.EditAplicacion(a);
        }
        [HttpPost]
        [Route("app/DeleteAplicacion")]
        public bool DeleteAplicacion(Entity.Aplicacion a)
        {
            return pAplicacionr.DeleteAplicacion(a);
        }
        [HttpPost]
        [Route("app/GetAplicacion")]
        public List<Entity.Aplicacion> GetAplicacion(Entity.Aplicacion a)
        {
            return pAplicacionr.GetAplicacion(a);
        }
        [HttpPost]
        [Route("app/GetAllAplicacion")]
        public List<Entity.Aplicacion> GetAllAplicacion()
        {
            return pAplicacionr.GetAllAplicacion();
        }

    }
}
