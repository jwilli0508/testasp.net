﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using testAspnet.BusinessLayer.Interface;
namespace testAspnet.Controllers
{
    public class UsuarioAppRolController : ApiController
    {
        private IUsuarioAppRol pUsuarioAppRol;
        public UsuarioAppRolController(IUsuarioAppRol pUsuarioAppRol)
        {
            this.pUsuarioAppRol = pUsuarioAppRol;
        }

        [HttpPost]
        [Route("app/AddUsuarioAppRol")]
        public bool AddUsuarioAppRol(Entity.UsuarioAppRol u)
        {
            return pUsuarioAppRol.AddUsuarioAppRol(u);
        }
        [HttpPost]
        [Route("app/EditUsuarioAppRol")]
        public bool EditUsuarioAppRol(Entity.UsuarioAppRol u)
        {
            return pUsuarioAppRol.EditUsuarioAppRol(u);
        }
        [HttpPost]
        [Route("app/DeleteUsuarioAppRol")]
        public bool DeleteUsuarioAppRol(Entity.UsuarioAppRol u)
        {
            return pUsuarioAppRol.DeleteUsuarioAppRol(u);
        }
        [HttpPost]
        [Route("app/GetUsuarioAppRol")]
        public List<Entity.UsuarioAppRol> GetUsuarioAppRol(Entity.UsuarioAppRol u)
        {
            return pUsuarioAppRol.GetUsuarioAppRol(u);
        }
        [HttpPost]
        [Route("app/GetAllUsuarioAppRol")]
        public List<Entity.UsuarioAppRol> GetAllUsuarioAppRol()
        {
            return pUsuarioAppRol.GetAllUsuarioAppRol();
        }
    }
}
