﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using testAspnet.BusinessLayer.Interface;
namespace testAspnet.Controllers
{
    public class RolesController : ApiController
    {
        private IRoles pRoles;
        public RolesController(IRoles pRoles)
        {
            this.pRoles = pRoles;
        }

        [HttpPost]
        [Route("app/AddRoles")]
        public bool AddRoles(Entity.Roles r)
        {
            return pRoles.AddRoles(r);
        }
        [HttpPost]
        [Route("app/EditRoles")]
        public bool EditRoles(Entity.Roles r)
        {
            return pRoles.EditRoles(r);
        }
        [HttpPost]
        [Route("app/DeleteRoles")]
        public bool DeleteRoles(Entity.Roles r)
        {
            return pRoles.DeleteRoles(r);
        }
        [HttpPost]
        [Route("app/GetRoles")]
        public List<Entity.Roles> GetRoles(Entity.Roles r)
        {
            return pRoles.GetRoles(r);
        }
        [HttpPost]
        [Route("app/GetAllRoles")]
        public List<Entity.Roles> GetAllRoles()
        {
            return pRoles.GetAllRoles();
        }
    }
}
