﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqKit;
using testAspnet.Entity;

namespace testAspnet.DAL
{
    public class UsuarioDAL
    {
        public UsuarioDAL()
        {
                
        }

        public bool AddUsuario(Usuario usuario)
        {
            try
            {
                using (var vModel = new ModelTestAspnet())
                {

                    vModel.Usuario.Add(usuario);
                    vModel.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool EditUsuario(Usuario usuario)
        {
            try
            {
                using (var vModel = new ModelTestAspnet())
                {

                    vModel.Usuario.AddOrUpdate(usuario);
                    vModel.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteUsuario(Usuario usuario)
        {
            try
            {
                using (var vModel = new ModelTestAspnet())
                {

                    vModel.Entry(usuario).State = System.Data.Entity.EntityState.Deleted;
                    vModel.SaveChanges();
                    
                    
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<Usuario> GetUsuario(Usuario usuario)
        {
            try
            {
                using (var vModel = new ModelTestAspnet())
                {
                    vModel.Configuration.ProxyCreationEnabled = false;
                    var vPredicado = PredicateBuilder.New<Usuario>();
                    if (usuario.Id_Usuario != 0) vPredicado.Or(x => x.Id_Usuario == usuario.Id_Usuario);
                    if (usuario.Nombres != null) vPredicado.Or(x => x.Nombres.Contains(usuario.Nombres));
                    return vModel.Usuario.Where(vPredicado).ToList();
                }
            }
            catch (Exception)
            {
                return new List<Usuario>();
            };
        }


        public List<Usuario> GetAllUsuario()
        {
            try
            {
                using (var vModel = new ModelTestAspnet())
                {
                    vModel.Configuration.ProxyCreationEnabled = false;
                    return vModel.Usuario.ToList();

                }

            }
            catch (Exception)
            {
                return new List<Usuario> ();
            }
        }
    }
}
