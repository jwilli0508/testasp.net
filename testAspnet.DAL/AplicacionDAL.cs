﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqKit;
using testAspnet.Entity;

namespace testAspnet.DAL
{
    public class AplicacionDAL
    {
        public AplicacionDAL()
        {
            
        }

        public bool AddAplicacion(Aplicacion app)
        {
            try
            {
                using (var vModel = new ModelTestAspnet())
                {
                    vModel.Aplicacion.Add(app);
                    vModel.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public bool EditAplicacion(Aplicacion app)
        {
            try
            {
                using (var vModel = new ModelTestAspnet())
                {
                    vModel.Aplicacion.AddOrUpdate(app);
                    vModel.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public bool DeleteAplicacion(Aplicacion app)
        {
            try
            {
                using (var vModel = new ModelTestAspnet())
                {
                    vModel.Entry(app).State = System.Data.Entity.EntityState.Deleted;
                    vModel.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {

                return false;
            }
        }

        public List<Aplicacion> GetAplicacion(Aplicacion app)
        {
            try
            {
                using (var vModel = new ModelTestAspnet())
                {
                    vModel.Configuration.ProxyCreationEnabled = false;
                    var vPredicado = PredicateBuilder.New<Aplicacion>();
                    if (app.Id_Aplicacion != 0) vPredicado.Or(x => x.Id_Aplicacion==app.Id_Aplicacion);
                    if (app.Descripcion!= null) vPredicado.Or(x => x.Descripcion.Contains(app.Descripcion));
                    return vModel.Aplicacion.Where(vPredicado).ToList();
                }
            }
            catch (Exception)
            {
                return new List<Aplicacion>();
            }
        }


        public List<Aplicacion> GetAllAplicacion()
        {
            try
            {
                using (var vModel = new ModelTestAspnet())
                {
                    vModel.Configuration.ProxyCreationEnabled = false;
                    return vModel.Aplicacion.Include("UsuarioAppRol").ToList();
                }
            }
            catch (Exception)
            {
                return new List<Aplicacion>();
            }
        }

    }
}
