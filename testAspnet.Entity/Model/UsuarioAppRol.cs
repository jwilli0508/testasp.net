namespace testAspnet.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UsuarioAppRol")]
    public partial class UsuarioAppRol
    {
        [Key]
        public int Id_UsuarioAppRol { get; set; }

        public int Id_Usuario { get; set; }

        public int Id_Roles { get; set; }

        public int Id_Aplicacion { get; set; }

        public virtual Aplicacion Aplicacion { get; set; }

        public virtual Roles Roles { get; set; }

        public virtual Usuario Usuario { get; set; }
    }
}
