namespace testAspnet.Entity
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ModelTestAspnet : DbContext
    {
        public ModelTestAspnet()
            : base("name=ModelTestAspnet")
        {
        }

        public virtual DbSet<Aplicacion> Aplicacion { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }
        public virtual DbSet<UsuarioAppRol> UsuarioAppRol { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Aplicacion>()
                .Property(e => e.Descripcion)
                .IsUnicode(false); 

            modelBuilder.Entity<Aplicacion>()
                .HasMany(e => e.UsuarioAppRol)
                .WithRequired(e => e.Aplicacion)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Roles>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Roles>()
                .HasMany(e => e.UsuarioAppRol)
                .WithRequired(e => e.Roles)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.Nombres)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .HasMany(e => e.UsuarioAppRol)
                .WithRequired(e => e.Usuario)
                .WillCascadeOnDelete(true);
        }
    }
}
