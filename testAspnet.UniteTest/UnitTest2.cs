﻿using System;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using  Moq;
using testAspnet.Controllers;
using testAspnet;
using testAspnet.BusinessLayer.Interface;
using testAspnet.Entity;

namespace testAspnet.UniteTest
{
    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void TestObtenerAplicaciones()
        {
            var mockValidacion = new Mock<IAplicacion>();
            mockValidacion.Setup(x => x.GetAllAplicacion());
            var controlador = new AplicacionController(mockValidacion.Object);
            controlador.GetAllAplicacion();
            mockValidacion.VerifyAll();
        }


        [TestMethod]
        public void TestAgregarAplicacion()
        {
            var mockRepository = new Mock<IAplicacion>();
            mockRepository
                .Setup(x => x.AddAplicacion(It.IsAny<Entity.Aplicacion>())).Returns(true);
            var controller = new AplicacionController(mockRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            var data = new Entity.Aplicacion(){Descripcion = "test agregar"};
            //Act            
            var response = controller.AddAplicacion(data);
            Assert.IsTrue(response);
            
        }
    }
}
