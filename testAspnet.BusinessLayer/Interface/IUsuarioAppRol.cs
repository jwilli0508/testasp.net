﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testAspnet;

namespace testAspnet.BusinessLayer.Interface
{
    public interface IUsuarioAppRol
    {
        bool AddUsuarioAppRol(Entity.UsuarioAppRol u);
        bool EditUsuarioAppRol(Entity.UsuarioAppRol u);
        bool DeleteUsuarioAppRol(Entity.UsuarioAppRol u);
        List<Entity.UsuarioAppRol> GetUsuarioAppRol(Entity.UsuarioAppRol u);
        List<Entity.UsuarioAppRol> GetAllUsuarioAppRol();
    }
}              
