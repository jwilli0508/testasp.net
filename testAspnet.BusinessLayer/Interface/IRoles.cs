﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testAspnet;

namespace testAspnet.BusinessLayer.Interface
{
    public interface IRoles
    {
        bool AddRoles(Entity.Roles r);
        bool EditRoles(Entity.Roles r);
        bool DeleteRoles(Entity.Roles r);
        List<Entity.Roles> GetRoles(Entity.Roles r);
        List<Entity.Roles> GetAllRoles();


    }
}
