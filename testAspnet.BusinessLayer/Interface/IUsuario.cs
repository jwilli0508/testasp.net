﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testAspnet;

namespace testAspnet.BusinessLayer.Interface
{
    public interface IUsuario
    {
        bool AddUsuario(Entity.Usuario u);



        bool EditUsuario(Entity.Usuario u);



        bool DeleteUsuario(Entity.Usuario u);



        List<Entity.Usuario> GetUsuario(Entity.Usuario u);




        List<Entity.Usuario> GetAllUsuario();


    }
}              
