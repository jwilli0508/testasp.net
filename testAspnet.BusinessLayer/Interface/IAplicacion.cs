﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testAspnet;

namespace testAspnet.BusinessLayer.Interface
{
    public interface IAplicacion
    {
        bool AddAplicacion(Entity.Aplicacion a);
        bool EditAplicacion(Entity.Aplicacion a);
        bool DeleteAplicacion(Entity.Aplicacion a);
        List<Entity.Aplicacion> GetAplicacion(Entity.Aplicacion a);
        List<Entity.Aplicacion> GetAllAplicacion();
    }
}
