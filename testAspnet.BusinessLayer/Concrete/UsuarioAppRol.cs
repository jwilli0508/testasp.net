﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testAspnet.BusinessLayer.Interface;
using testAspnet;
using testAspnet.DAL;

namespace testAspnet.BusinessLayer.Concrete
{
    public class UsuarioAppRol: IUsuarioAppRol
    {
        public bool AddUsuarioAppRol(Entity.UsuarioAppRol u)
        {
            return new UsuarioAppRolDAL().AddUsuarioAppRol(u);
        }

        public bool EditUsuarioAppRol(Entity.UsuarioAppRol u)
        {
            return new UsuarioAppRolDAL().EditUsuarioAppRol(u);
        }

        public bool DeleteUsuarioAppRol(Entity.UsuarioAppRol u)
        {
            return new UsuarioAppRolDAL().DeleteUsuarioAppRol(u);
        }

        public List<Entity.UsuarioAppRol> GetUsuarioAppRol(Entity.UsuarioAppRol u)
        {
            return new UsuarioAppRolDAL().GetUsuarioAppRol(u);
        }

        public List<Entity.UsuarioAppRol> GetAllUsuarioAppRol()
        {
            return new UsuarioAppRolDAL().GetAllUsuarioAppRol();
        }
    }
}
