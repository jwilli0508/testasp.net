﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testAspnet.BusinessLayer.Interface;
using testAspnet.DAL;

namespace testAspnet.BusinessLayer.Concrete
{
    public class Usuario: IUsuario

    {
        public bool AddUsuario(Entity.Usuario u)
        {
            return new UsuarioDAL().AddUsuario(u);
        }

        public bool EditUsuario(Entity.Usuario u)
        {
            return new UsuarioDAL().EditUsuario(u);
        }

        public bool DeleteUsuario(Entity.Usuario u)
        {
            return new UsuarioDAL().DeleteUsuario(u);
        }

        public List<Entity.Usuario> GetUsuario(Entity.Usuario u)
        {
            return new UsuarioDAL().GetUsuario(u);
        }

        public List<Entity.Usuario> GetAllUsuario()
        {
            return new UsuarioDAL().GetAllUsuario();
        }
    }
}
