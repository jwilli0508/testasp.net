﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testAspnet.BusinessLayer.Interface;
using testAspnet;

namespace testAspnet.BusinessLayer.Concrete
{
    public class Aplicacion:IAplicacion
    {
        public bool AddAplicacion(Entity.Aplicacion a)
        {
            return new testAspnet.DAL.AplicacionDAL().AddAplicacion(a);
        }

        public bool EditAplicacion(Entity.Aplicacion a)
        {
            return new testAspnet.DAL.AplicacionDAL().EditAplicacion(a);
        }

        public bool DeleteAplicacion(Entity.Aplicacion a)
        {
            return new testAspnet.DAL.AplicacionDAL().DeleteAplicacion(a);
        }

        public List<Entity.Aplicacion> GetAplicacion(Entity.Aplicacion a)
        {
            return new testAspnet.DAL.AplicacionDAL().GetAplicacion(a);
        }

        public List<Entity.Aplicacion> GetAllAplicacion()
        {
            return new testAspnet.DAL.AplicacionDAL().GetAllAplicacion();
        }
    }
}
