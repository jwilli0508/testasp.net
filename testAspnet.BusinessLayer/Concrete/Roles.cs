﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testAspnet.BusinessLayer.Interface;
using testAspnet;

namespace testAspnet.BusinessLayer.Concrete
{
    public class Roles:IRoles
    {
        public bool AddRoles(Entity.Roles r)
        {
            return new testAspnet.DAL.RolesDAL().AddRoles(r);
        }

        public bool EditRoles(Entity.Roles r)
        {
            return new testAspnet.DAL.RolesDAL().EditRoles(r);
        }

        public bool DeleteRoles(Entity.Roles r)
        {
            return new testAspnet.DAL.RolesDAL().DeleteRoles(r);
        }

        public List<Entity.Roles> GetRoles(Entity.Roles r)
        {
            return new testAspnet.DAL.RolesDAL().GetRoles(r);
        }

        public List<Entity.Roles> GetAllRoles()
        {
            return new testAspnet.DAL.RolesDAL().GetAllRoles();
        }
    }
}
